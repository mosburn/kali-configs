#!/bin/bash

wget http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.1%20x64.tar.bz2
tar xvjf Sublime\ Text\ 2.0.1\ x64.tar.bz2 -C /usr/local/share/
echo "/usr/local/share/Sublime\ Text\ 2/sublime_text" > /usr/local/bin/sublime
chmod +x /usr/local/bin/sublime
mkdir -p ~/.config
git clone git@bitbucket.org:mosburn/sublime-configs.git ~/.config/sublime-text-2/

